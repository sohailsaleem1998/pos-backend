﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace backend.Controllers
{
    public class SaleController : ApiController
    {
        public HttpResponseMessage Get(int id)
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);


            DataTable dt = new DataTable();
            string query = "Select * from Inventory where I_Id = '" + id + "'";
            using (conn)
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    using (var sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        sda.Fill(dt);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, dt);

        }
        public string Put(Inventory inventory)
        {
            try
            {
                var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);
                DataTable dt = new DataTable();
                int productQuantity = 0;
                string returnVal = "";
                string qtyQuery = "Select I_Quantity from Inventory where I_Id = '" + inventory.I_Id + "'";
                using (conn)
                {
                    
                    SqlCommand cmdQty = new SqlCommand(qtyQuery, conn);
                    conn.Open();
                    productQuantity = (Int32)cmdQty.ExecuteScalar();
                    if (inventory.I_Quantity > productQuantity)
                    {
                        returnVal = "Out of Products!";
                    }
                    else
                    {
                        string query = "Update Inventory Set I_Quantity= I_Quantity - '" + inventory.I_Quantity + "' where I_Id='" + inventory.I_Id + "'";
                        string queryRevenue = "Update Revenue Set Funds= Funds + '" + inventory.I_Price * inventory.I_Quantity+ "' where R_Id=1";
                        using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                        {
                            using (var cmd = new SqlCommand(query, con))
                            {
                                using (var sda = new SqlDataAdapter(cmd))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    sda.Fill(dt);
                                }
                            }
                        }
                        using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                        {
                            using (var cmd = new SqlCommand(queryRevenue, con))
                            {
                                using (var sda = new SqlDataAdapter(cmd))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    sda.Fill(dt);
                                }
                            }
                        }
                        returnVal = "Product is sold!";
                    }
                }

                return returnVal;
            }
            catch (Exception err)
            {

                throw err;
            }
        }
    }
}
