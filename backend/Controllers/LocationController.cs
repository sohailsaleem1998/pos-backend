﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace backend.Controllers
{
    public class LocationController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);
            DataTable dt = new DataTable();
            string query = "Select * from Location";
            using (conn)
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    using (var sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        sda.Fill(dt);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, dt);
        }
        public string Post(Location location)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Insert into Location Values('" + location.Loc_Name + "','" + location.Loc_Address + "','" + location.Loc_Cell + "')";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Added Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
        public string Put(Location location)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Update Location Set Loc_Name='" + location.Loc_Name + "', Loc_Address='" + location.Loc_Address + "',Loc_Cell= '" + location.Loc_Cell + "' where Loc_Id='" + location.Loc_Id + "'";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Updated Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
        public string Delete(Location location)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Delete Location Where Loc_Id='" + location.Loc_Id + "'";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Deleted Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
    }
}
