﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace backend.Controllers
{
    public class SalaryController : ApiController
    {
        public string Put(Employee emp)
        {
            try
            {
                var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);
                DataTable dt = new DataTable();
                Decimal Funds = 0;
                string returnVal = "";
                var fundQuery = "select Funds from Revenue where R_Id = 1";
                using(conn){
                    SqlCommand cmd = new SqlCommand(fundQuery, conn);
                    conn.Open();
                    Funds = (Decimal)cmd.ExecuteScalar();
                    if(emp.E_Salary > Funds)
                    {
                        returnVal = "Out Of Funds!";
                    }
                    else
                    {
                        string query = "Update Revenue Set Funds= Funds - '" + emp.E_Salary + "' where R_Id=1";
                        using (conn)
                        {
                            SqlCommand cmdQuery = new SqlCommand(query, conn);
                            using (cmdQuery)
                            {
                                using (var sda = new SqlDataAdapter(cmdQuery))
                                {
                                    cmdQuery.CommandType = CommandType.Text;
                                    sda.Fill(dt);
                                    returnVal = "Salary have been transfered!";
                                }
                            }
                        }
                    }
                }
                return returnVal;
            }
            catch (Exception err)
            {

                throw err;
            }
        }
    }
}
