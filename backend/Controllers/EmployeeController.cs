﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using backend.Models;

namespace backend.Controllers
{
    public class EmployeeController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);
  
            
                DataTable dt = new DataTable();
                string query = "Select * from Employee";
                using (conn)
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, dt);

            }
        public HttpResponseMessage Get(int id)
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);


            DataTable dt = new DataTable();
            string query = "Select * from Employee where E_Id = '"+id+"'";
            using (conn)
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    using (var sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        sda.Fill(dt);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, dt);

        }       
        public string Post(Employee employee)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Insert into Employee Values('" + employee.E_Name + "','" + employee.E_Depart + "','" + employee.E_Salary + "')";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Added Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
        public string Put(Employee employee)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Update Employee Set E_Name='" + employee.E_Name + "', E_Depart='" + employee.E_Depart + "',E_Salary= '" + employee.E_Salary + "' where E_Id='" + employee.E_Id + "'";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Updated Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
        public string Delete(int id)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Delete Employee Where E_Id='" + id + "'";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Deleted Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }

    }
}
