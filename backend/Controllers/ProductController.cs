﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace backend.Controllers
{
    public class ProductController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);
            DataTable dt = new DataTable();
            Inventory inv = new Inventory();
            string query = "Select * from Inventory where isProduct= 1";
            using (conn)
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    using (var sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        sda.Fill(dt);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, dt);
        }
    }
}
