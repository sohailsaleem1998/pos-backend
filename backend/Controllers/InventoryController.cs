﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace backend.Controllers
{
    public class InventoryController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);
            DataTable dt = new DataTable();
            string query = "Select * from Inventory";
            using (conn)
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    using (var sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        sda.Fill(dt);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, dt);
        }
        public HttpResponseMessage Get(int id)
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString);


            DataTable dt = new DataTable();
            string query = "Select * from Inventory where I_Id = '" + id + "'";
            using (conn)
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    using (var sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.Text;
                        sda.Fill(dt);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, dt);

        }       
        public string Post(Inventory inventory)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Insert into Inventory Values('" + inventory.I_Name + "','" + inventory.isProduct + "','" + inventory.I_Price + "','"+ inventory.I_Quantity+"')";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Added Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
        public string Put(Inventory inventory)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Update Inventory Set I_Name='" + inventory.I_Name + "', isProduct='" + inventory.isProduct + "',I_Price= '" + inventory.I_Price + "',I_Quantity= '" + inventory.I_Quantity + "' where I_Id='" + inventory.I_Id + "'";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Updated Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
        public string Delete(int id)
        {
            try
            {
                DataTable dt = new DataTable();
                string query = "Delete Inventory Where I_Id='" + id + "'";
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["pos"].ConnectionString))
                {
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        using (var sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.Text;
                            sda.Fill(dt);
                        }
                    }
                }
                return "Data Deleted Successfully!";
            }
            catch (Exception err)
            {

                throw err;
            }
        }
    }
}
