﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backend.Models
{
    public class Revenue
    {
        public int R_Id { get; set; }
        public decimal Funds { get; set; }
    }
}