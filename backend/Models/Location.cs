﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backend.Models
{
    public class Location
    {
        public int Loc_Id { get; set; }
        public string Loc_Name { get; set; }
        public string Loc_Address { get; set; }
        public string Loc_Cell { get; set; }
    }
}