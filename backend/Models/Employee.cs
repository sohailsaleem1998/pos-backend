﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backend.Models
{
    public class Employee
    {
        public int E_Id { get; set; }
        public string E_Name { get; set; }
        public string E_Depart { get; set; }
        public decimal E_Salary { get; set; }
    }
}