﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backend.Models
{
    public class Inventory
    {
        public int I_Id { get; set; }
        public string I_Name { get; set; }
        public int isProduct { get; set; }
        public decimal I_Price { get; set; }
        public int I_Quantity { get; set; }
    }
}